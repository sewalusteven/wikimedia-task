# wikimedia-task

This is the assignment for the wikimedia foundation given to Sewalu Mukasa Steven

## Instructions on running the Project

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```

### Note

Given I was unsure about the requirement around the multiple wikipedia sites, I set up a json to act as my central repository for the various
sub domains representing all the sites under wikipedia. i then fetch them through the store and create a select drop down for it