import {defineStore} from 'pinia'
import wikis from "./wikipedia_sites.json"
import axios from "axios";

export const useWikiStore = defineStore('wiki', {
    state: () => ({
        languages: wikis,
        subDomain: "en",
        perPage: 10,
        results: []
    }),
    getters: {
        apiUrl: (state) => `https://${state.subDomain}.wikipedia.org/w/rest.php/v1/search/title?limit=${state.perPage}`
    },
    actions: {
        changeSubDomain(sub) {
            this.subDomain = sub;
        },
        resetResults(){
            this.results = []
            this.perPage = 10
            this.subDomain = "en"
        },
        setResultNumber(limit){
            this.perPage = limit
        },
        async searchResults(query) {
            const {data} = await axios.get(`${this.apiUrl}&q=${query}`)

            if (data && data.pages) {
                this.results = data.pages.map(p => {
                    return {
                        id: p.id,
                        title: p.title,
                        description: p.description,
                        thumb: p.thumbnail ? p.thumbnail.url : null
                    }
                })
            }
        }

    }
})
